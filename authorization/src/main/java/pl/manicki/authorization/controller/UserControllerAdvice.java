package pl.manicki.authorization.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.manicki.authorization.exception.UserAlreadyExistsException;
import pl.manicki.commons.exception.IncorrectUserRoleException;
import pl.manicki.commons.exception.UserNotFoundException;

@ControllerAdvice
public class UserControllerAdvice {

    private static final Logger logger = LoggerFactory.getLogger(UserControllerAdvice.class);

    @ExceptionHandler({IncorrectUserRoleException.class, UserAlreadyExistsException.class,
            UserNotFoundException.class})
    public ResponseEntity<String> badRequestExceptions(final Exception e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<Object> validationException(final MethodArgumentNotValidException e) {
        logger.warn("{} - {}", e.getClass().getSimpleName(), e.getAllErrors());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getAllErrors().get(0).getDefaultMessage());
    }
}
