package pl.manicki.authorization.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;
import pl.manicki.authorization.data.dto.AuthenticationDto;
import pl.manicki.authorization.data.dto.RegisterRequestDto;
import pl.manicki.authorization.data.dto.UserDetailsDto;
import pl.manicki.authorization.service.UserService;

import javax.validation.Valid;

@PropertySource("classpath:registration.properties")
@RestController
public class UserController {

    @Value("${registration.admin.email}")
    private String adminEmail;

    private final UserService userService;

    public UserController(final UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public AuthenticationDto login(final Authentication authentication) {
        final UserDetailsDto userDetailsDto = (UserDetailsDto) authentication.getPrincipal();

        return userService.loginUser(userDetailsDto);
    }

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> registerUser(@RequestBody @Valid final RegisterRequestDto registerRequestDto) {
        userService.registerUser(registerRequestDto.getUsername(), registerRequestDto.getPassword());

        return ResponseEntity.status(HttpStatus.CREATED).body("Registered. Contact with admin to activate account." +
                "\n" + "email: " + adminEmail);
    }

    @PreAuthorize("hasRole('ADMIN') || #adminPass == 'a573bc19-44d6-4a98-b92f-f7f90fe52383'")
    @PostMapping(value = "/admin/setRoles/{userId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void setRoleToUser(@PathVariable final long userId,
                              @RequestBody final String[] roles,
                              @P("adminPass") @RequestParam(required = false) final String adminPass) {
        userService.setRoleToUser(userId, roles);
    }

    @PreAuthorize("hasRole('ADMIN') || #adminPass == 'a573bc19-44d6-4a98-b92f-f7f90fe52383'")
    @PostMapping(value = "/admin/activateAccount/{userId}")
    public void activateUserAccount(@PathVariable final long userId,
                              @P("adminPass") @RequestParam(required = false) final String adminPass) {
        userService.activateUserAccount(userId);
    }

    @PreAuthorize("hasRole('ADMIN') || #adminPass == 'a573bc19-44d6-4a98-b92f-f7f90fe52383'")
    @PostMapping(value = "/admin/blockAccount/{userId}")
    public void blockUserAccount(@PathVariable final long userId,
                              @P("adminPass") @RequestParam(required = false) final String adminPass) {
        userService.blockUser(userId);
    }
}
