package pl.manicki.authorization.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.manicki.commons.data.entity.UserEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findByUsername(final String username);

    List<UserEntity> findAllByActiveAndTokenValidityDateTimeAfter(final boolean active, final LocalDateTime dateTime);

    boolean existsByUsername(final String username);
}
