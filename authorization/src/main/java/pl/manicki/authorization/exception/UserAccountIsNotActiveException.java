package pl.manicki.authorization.exception;

public class UserAccountIsNotActiveException extends RuntimeException {

    public UserAccountIsNotActiveException(String message) {
        super(message);
    }
}
