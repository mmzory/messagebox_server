package pl.manicki.authorization.data.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterRequestDto {
    @NotEmpty(message = "Username has to be provided!")
    @NotBlank(message = "Username has to have no whitespace characters!")
    @Size(min = 4, max = 40, message = "Username length has to be between 4 - 40 signs!")
    private String username;

    @NotEmpty(message = "Password has to be provided!")
    @NotBlank(message = "Password has to have no whitespace characters!")
    @Size(min = 6, message = "Password length has to have more than 6 signs!")
    private String password;
}
