package pl.manicki.authorization.data.dto;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.manicki.commons.data.dto.UserDto;
import pl.manicki.commons.encoder.UserRolesEncoder;

import java.util.Collection;

public class UserDetailsDto implements UserDetails {

    private final UserDto userDto;

    public UserDetailsDto(final UserDto userDto) {
        this.userDto = userDto;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new UserRolesEncoder().decodeRoles(userDto.getRoleCode());
    }

    @Override
    public String getPassword() {
        return userDto.getPassword();
    }

    @Override
    public String getUsername() {
        return userDto.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return !userDto.isBlocked();
    }

    @Override
    public boolean isAccountNonLocked() {
        return !userDto.isBlocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !userDto.isBlocked();
    }

    @Override
    public boolean isEnabled() {
        return userDto.isActive();
    }
}
