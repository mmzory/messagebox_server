package pl.manicki.authorization.service;

import pl.manicki.authorization.data.dto.AuthenticationDto;
import pl.manicki.authorization.data.dto.UserDetailsDto;
import pl.manicki.commons.data.entity.UserEntity;

public interface UserService {
    AuthenticationDto loginUser(final UserDetailsDto userDetailsDto);
    void registerUser(final String username, final String password);
    void setRoleToUser(final long userId, final String... roles);
    void activateUserAccount(final long userId);
    void blockUser(final long userId);
    UserEntity findByUsername(final String username);
}
