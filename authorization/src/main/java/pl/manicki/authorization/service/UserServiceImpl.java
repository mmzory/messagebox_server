package pl.manicki.authorization.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.manicki.authorization.data.dto.AuthenticationDto;
import pl.manicki.authorization.data.dto.UserDetailsDto;
import pl.manicki.authorization.exception.UserAccountIsNotActiveException;
import pl.manicki.authorization.exception.UserAlreadyExistsException;
import pl.manicki.authorization.repository.UserRepository;
import pl.manicki.commons.data.dto.AuthorizationDetails;
import pl.manicki.commons.data.dto.UserDto;
import pl.manicki.commons.data.entity.UserEntity;
import pl.manicki.commons.data.mapper.UserMapper;
import pl.manicki.commons.encoder.UserRolesEncoder;
import pl.manicki.commons.exception.UserNotFoundException;
import pl.manicki.commons.service.UserTokenService;
import pl.manicki.commons.service.UserTokenServiceImpl;
import pl.manicki.commons.util.JwtHelper;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private final int DEFAULT_ROLE_CODE = 1;
    private final boolean USER_ACCOUNT_NOT_ACTIVE = false;
    private final boolean USER_ACCOUNT_NOT_BLOCKED = false;

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    private final UserRolesEncoder userRolesEncoder;
    private final UserTokenService userTokenService;

    public UserServiceImpl(UserRepository userRepository,
                           PasswordEncoder passwordEncoder,
                           UserRolesEncoder userRolesEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRolesEncoder = userRolesEncoder;
        this.userTokenService = new UserTokenServiceImpl(userRolesEncoder);

        saveActiveUsersWithValidToken();
    }

    @Transactional
    @Override
    public AuthenticationDto loginUser(final UserDetailsDto userDetailsDto) {
        logger.info("User with username = '{}' is logging to system.", userDetailsDto.getUsername());
        final UserEntity userEntity = findByUsername(userDetailsDto.getUsername());

        if (!userEntity.isActive()) {
            logger.warn("Account of user with ID = {} is not active.", userEntity.getId());
            throw new UserAccountIsNotActiveException("User account is not active. Contact an administrator.");
        }

        final UserDto userDto = UserMapper.mapEntityToDtoForJwtToken(userEntity);
        final AuthorizationDetails userToken = userTokenService.generateUserToken(userDto);
        userTokenService.setTokenToUser(userEntity, userToken);
        userRepository.save(userEntity);
        final String authenticationToken = new JwtHelper().createAuthenticationToken(userDto, userToken);

        return new AuthenticationDto(userDto.getId(), authenticationToken);
    }

    @Override
    public void registerUser(final String username, final String password) {
        checkIfUsernameIsUnique(username);

        userRepository.save(
                new UserEntity(
                        username,
                        passwordEncoder.encode(password),
                        null,
                        LocalDateTime.now(),
                        null,
                        USER_ACCOUNT_NOT_ACTIVE,
                        USER_ACCOUNT_NOT_BLOCKED,
                        DEFAULT_ROLE_CODE));
    }

    @Transactional
    @Override
    public void setRoleToUser(final long userId, final String... roles) {
        UserEntity user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(String.format("User with id = %d not found.", userId)));

        user.setRole(userRolesEncoder.encodeRoles(roles));

        userRepository.save(user);
    }

    @Transactional
    @Override
    public void activateUserAccount(final long userId) {
        UserEntity userEntity = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(String.format("User with id = %d not found.", userId)));

        userEntity.setActive(true);

        userRepository.save(userEntity);
    }

    @Transactional
    @Override
    public void blockUser(final long userId) {
        UserEntity userEntity = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(String.format("User with id = %d not found.", userId)));

        userEntity.setBlocked(true);

        userRepository.save(userEntity);
    }

    @Override
    public UserEntity findByUsername(final String username) throws RuntimeException {
        UserEntity userEntity = userRepository.findByUsername(username).orElse(null);
        if (userEntity == null) {
            throw new UserNotFoundException("Incorrect username or password.");
        }

        return userEntity;
    }

    private void checkIfUsernameIsUnique(final String username) throws UserAlreadyExistsException {
        if (userRepository.existsByUsername(username)) {
            logger.warn("Account with username = {} is already exists.", username);
            throw new UserAlreadyExistsException(String.format("User with login '%s' already exists.", username));
        }
    }

    private void saveActiveUsersWithValidToken() {
        logger.info("Reading active users with valid token from DB...");

        final List<UserEntity> activeUsersWithValidToken = userRepository.findAllByActiveAndTokenValidityDateTimeAfter(true, LocalDateTime.now());
        activeUsersWithValidToken.forEach(user -> {
            final AuthorizationDetails authorizationDetails = new AuthorizationDetails(
                    user.getToken(),
                    user.getTokenValidityDateTime(),
                    userRolesEncoder.decodeRoles(user.getRole())
            );

            userTokenService.saveUserAndToken(user.getId(), authorizationDetails);
        });

        logger.info("Reading active users with valid token from DB finished.");
    }
}
