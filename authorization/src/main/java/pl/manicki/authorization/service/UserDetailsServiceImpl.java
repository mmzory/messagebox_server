package pl.manicki.authorization.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import pl.manicki.authorization.data.dto.UserDetailsDto;
import pl.manicki.authorization.exception.UserAccountIsNotActiveException;
import pl.manicki.commons.data.mapper.UserMapper;
import pl.manicki.commons.exception.UserNotFoundException;

@Component("customUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;

    public UserDetailsServiceImpl(final UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UserNotFoundException {
        final UserDetailsDto userDetailsDto =
                new UserDetailsDto(UserMapper.mapRegisterUserEntityToDto(userService.findByUsername(username)));

        if (!userDetailsDto.isEnabled()) {
            throw new UserAccountIsNotActiveException("User account has not been activated.");
        }

        return userDetailsDto;
    }
}
