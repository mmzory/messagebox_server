package pl.manicki.authorization.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.manicki.authorization.repository.UserRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@PropertySource("classpath:registration.properties")
@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
class UserRegistrationTest {

    private final MockMvc mockMvc;
    private final UserRepository userRepository;

    private final String username = "testUser";
    private final String password = "testPassword";

    @Autowired
    public UserRegistrationTest(MockMvc mockMvc,
                                UserRepository userRepository) {
        this.mockMvc = mockMvc;
        this.userRepository = userRepository;
    }

    @AfterEach
    public void afterEach() {
        userRepository.deleteAll();
    }

    @Test
    public void shouldCorrectlyRegisterUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\" : \"" + username + "\", \"password\" : \"" + password + "\"}"))
            .andExpect(MockMvcResultMatchers.status().isCreated());

        assertTrue(userRepository.existsByUsername(username));
    }

    @Test
    public void shouldResponseThatUserAlreadyExists() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\" : \"" + username + "\", \"password\" : \"" + password + "\"}"))
                .andExpect(MockMvcResultMatchers.status().isCreated());

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\" : \"" + username + "\", \"password\" : \"" + password + "\"}"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());

        assertEquals(String.format("User with login '%s'" +
                " already exists.", username), resultActions.andReturn().getResponse().getContentAsString());
    }

    @Test
    public void shouldThrowExceptionIfUsernameIsMissed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("\", \"password\" : \"" + password + "\"}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void shouldThrowExceptionIfUserHasWhitespaceUsername() throws Exception {
        final String whitespaceUsername = "          ";

        mockMvc.perform(MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\" : \"" + whitespaceUsername + "\", \"password\" : \"" + password + "\"}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void shouldThrowExceptionIfUserHasToShortUsername() throws Exception {
        final String shortUsername = "te";

        mockMvc.perform(MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\" : \"" + shortUsername + "\", \"password\" : \"" + password + "\"}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void shouldThrowExceptionIfUserHasToLongUsername() throws Exception {
        final String longUsername = "0123456789012345678901234567890123456789012";

        mockMvc.perform(MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\" : \"" + longUsername + "\", \"password\" : \"" + password + "\"}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void shouldThrowExceptionIfPasswordIsMissed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("\", \"username\" : \"" + username + "\"}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void shouldThrowExceptionIfUserHasWhitespacePassword() throws Exception {
        final String whitespacePassword = "          ";

        mockMvc.perform(MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\" : \"" + username + "\", \"password\" : \"" + whitespacePassword + "\"}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void shouldThrowExceptionIfUserHasToShortPassword() throws Exception {
        final String shortPassword = "te";

        mockMvc.perform(MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\" : \"" + username + "\", \"password\" : \"" + shortPassword + "\"}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}