package pl.manicki.authorization.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.manicki.authorization.repository.UserRepository;
import pl.manicki.commons.data.entity.UserEntity;

import static org.junit.jupiter.api.Assertions.*;

@PropertySource("classpath:registration.properties")
@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
class UserAccountActivationTest {

    private final MockMvc mockMvc;
    private final UserRepository userRepository;

    private final String username = "testUser";
    private final String password = "testPassword";

    private final String adminSecretPassword = "a573bc19-44d6-4a98-b92f-f7f90fe52383";
    private final String incorrectAdminSecretPassword = "88888888-4444-4444-4444-121212121212";

    @Autowired
    public UserAccountActivationTest(MockMvc mockMvc,
                                     UserRepository userRepository) {
        this.mockMvc = mockMvc;
        this.userRepository = userRepository;
    }

    @AfterEach
    public void afterEach() {
        userRepository.deleteAll();
    }

    @Test
    public void shouldCorrectlyActivateUserAccountIfSecretPasswordIsCorrect() throws Exception {
        long userId = registerUser();

        mockMvc.perform(MockMvcRequestBuilders.post("/admin/activateAccount/" + userId + "?adminPass=" + adminSecretPassword))
            .andExpect(MockMvcResultMatchers.status().isOk());

        final UserEntity userEntity = userRepository.findById(userId).orElse(null);

        assertNotNull(userEntity);
        assertTrue(userEntity.isActive());
    }

    @Test
    public void shouldNotActivateUserAccountIfSecretAdminPasswordIsMissed() throws Exception {
        long userId = registerUser();

        mockMvc.perform(MockMvcRequestBuilders.post("/admin/activateAccount/" + userId))
            .andExpect(MockMvcResultMatchers.status().isUnauthorized());

        final UserEntity userEntity = userRepository.findById(userId).orElse(null);

        assertNotNull(userEntity);
        assertFalse(userEntity.isActive());
    }

    @Test
    public void shouldNotActivateUserAccountIfSecretAdminPasswordIsIncorrect() throws Exception {
        final long userId = registerUser();

        mockMvc.perform(MockMvcRequestBuilders.post("/admin/activateAccount/" + userId + "?adminPass=" + incorrectAdminSecretPassword))
            .andExpect(MockMvcResultMatchers.status().isUnauthorized());

        final UserEntity userEntity = userRepository.findById(userId).orElse(null);

        assertNotNull(userEntity);
        assertFalse(userEntity.isActive());
    }

    @Test
    public void shouldThrowAnExceptionIfUserAccountDoesNotExists() throws Exception {
        final long notExistingUserId = -5L;

        mockMvc.perform(MockMvcRequestBuilders.post("/admin/activateAccount/" + notExistingUserId + "?adminPass=" + adminSecretPassword))
            .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @WithMockUser(roles = "ADMIN")
    @Test
    public void shouldCorrectlyActivateUserAccountIfUserHasAdminRole() throws Exception {
        final long userId = registerUser();

        mockMvc.perform(MockMvcRequestBuilders.post("/admin/activateAccount/" + userId))
            .andExpect(MockMvcResultMatchers.status().isOk());

        final UserEntity userEntity = userRepository.findById(userId).orElse(null);

        assertNotNull(userEntity);
        assertTrue(userEntity.isActive());
    }

    @WithMockUser(roles = "ADMIN")
    @Test
    public void shouldCorrectlyActivateUserAccountIfUserHasAdminRoleWithIncorrectAdminSecretPassword() throws Exception {
        final long userId = registerUser();

        mockMvc.perform(MockMvcRequestBuilders.post("/admin/activateAccount/" + userId + "?adminPass=" + incorrectAdminSecretPassword))
                .andExpect(MockMvcResultMatchers.status().isOk());

        final UserEntity userEntity = userRepository.findById(userId).orElse(null);

        assertNotNull(userEntity);
        assertTrue(userEntity.isActive());
    }

    @WithMockUser(roles = "USER")
    @Test
    public void shouldActivateUserAccountIfUserDoesNotHaveAdminRoleButProvideAdminSecretPassword() throws Exception {
        final long userId = registerUser();

        mockMvc.perform(MockMvcRequestBuilders.post("/admin/activateAccount/" + userId + "?adminPass=" + adminSecretPassword))
                .andExpect(MockMvcResultMatchers.status().isOk());

        final UserEntity userEntity = userRepository.findById(userId).orElse(null);

        assertNotNull(userEntity);
        assertTrue(userEntity.isActive());
    }

    @WithMockUser(roles = "USER")
    @Test
    public void shouldNotActivateUserAccountIfUserDoesNotHaveAdminRole() throws Exception {
        final long userId = registerUser();

        mockMvc.perform(MockMvcRequestBuilders.post("/admin/activateAccount/" + userId))
                .andExpect(MockMvcResultMatchers.status().isForbidden());

        final UserEntity userEntity = userRepository.findById(userId).orElse(null);

        assertNotNull(userEntity);
        assertFalse(userEntity.isActive());
    }

    public long registerUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\" : \"" + username + "\", \"password\" : \"" + password + "\"}"));

        return userRepository.findByUsername(username).map(UserEntity::getId).orElse(-1L);
    }
}