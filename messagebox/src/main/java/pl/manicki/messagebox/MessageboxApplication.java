package pl.manicki.messagebox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan("pl.manicki.commons.data.entity")
@SpringBootApplication
public class MessageboxApplication {

    public static void main(String[] args) {
        SpringApplication.run(MessageboxApplication.class, args);
    }

}
