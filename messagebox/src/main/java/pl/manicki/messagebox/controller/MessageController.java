package pl.manicki.messagebox.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.manicki.messagebox.data.dto.MessageDto;
import pl.manicki.messagebox.service.MessageService;
import pl.manicki.messagebox.service.MessageServiceImpl;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/message")
public class MessageController {
    private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
    private final MessageService messageService;

    public MessageController(MessageServiceImpl messageService) {
        this.messageService = messageService;
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MessageDto> getAllMessagesByUserId(HttpServletResponse response) {
        long userId = Long.parseLong(response.getHeader("userId"));
        logger.info("Get all messages for user id = {}", userId);
        return messageService.getListOfBasicMessagesDetailsFromUser(userId);
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/{messageId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public MessageDto getMessage(@PathVariable long messageId,
                                 HttpServletResponse response) {
        long userId = Long.parseLong(response.getHeader("userId"));
        logger.info("Get message for user id = {} with message id = {}", userId, messageId);
        return messageService.getMessage(userId, messageId);
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void sendMessage(@Valid @RequestBody MessageDto messageDto,
                            HttpServletResponse response) {
        String deviceIp = response.getHeader("deviceIp");
        long userId = Long.parseLong(response.getHeader("userId"));
        logger.info("User with ID = {} is sending message from IP = {}.", userId, deviceIp);
        messageService.saveMessage(userId, deviceIp, messageDto);
        response.setStatus(HttpStatus.CREATED.value());
    }

    @PreAuthorize("hasRole('USER')")
    @DeleteMapping(value = "/{messageId}")
    public void deleteMessage(@PathVariable long messageId,
                              HttpServletResponse response) {
        long userId = Long.parseLong(response.getHeader("userId"));
        logger.info("Delete message for user id = {} with message id = {}", userId, messageId);
        messageService.setMessageAsDeleted(userId, messageId);
    }
}
