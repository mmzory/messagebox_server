package pl.manicki.messagebox.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.manicki.messagebox.exception.AuthenticationException;
import pl.manicki.messagebox.exception.MessageNotFoundException;

@ControllerAdvice
public class MessageControllerAdvice {
    public static final Logger logger = LoggerFactory.getLogger(MessageControllerAdvice.class);

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<Object> validationException(final MethodArgumentNotValidException e) {
        logger.warn("{} - {}", e.getClass().getSimpleName(), e.getAllErrors());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getAllErrors().get(0).getDefaultMessage());
    }

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<Object> forbiddenRequestExceptions(final AuthenticationException e) {
        logger.warn("{} - {}", e.getClass().getSimpleName(), e.getMessage());
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    @ExceptionHandler({MessageNotFoundException.class})
    public ResponseEntity<Object> badRequestExceptions(final MessageNotFoundException e) {
        logger.warn("{} - {}", e.getClass().getSimpleName(), e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}
