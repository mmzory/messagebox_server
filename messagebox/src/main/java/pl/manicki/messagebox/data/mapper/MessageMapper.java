package pl.manicki.messagebox.data.mapper;

import pl.manicki.commons.data.entity.MessageEntity;
import pl.manicki.commons.data.entity.UserEntity;
import pl.manicki.messagebox.data.dto.MessageDto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class MessageMapper {

    private static final boolean MESSAGE_WAS_READ = false;
    private static final boolean MESSAGE_WAS_DELETED = false;

    public static List<MessageDto> mapBasicListOfMessageEntityToDto(final List<MessageEntity> messageEntityList) {
        return messageEntityList.stream().map(MessageMapper::mapBasicMessageEntityToDto).collect(Collectors.toList());
    }

    public static MessageDto mapBasicMessageEntityToDto(final MessageEntity messageEntity) {
        return new MessageDto(messageEntity.getId(),
                messageEntity.getTopic(),
                messageEntity.getRecipientId(),
                messageEntity.getSender().getId(),
                messageEntity.getSendDatetime(),
                messageEntity.wasRead());
    }

    public static MessageDto mapMessageEntityToDto(final MessageEntity messageEntity) {
        return new MessageDto(messageEntity.getId(),
                messageEntity.getTopic(),
                messageEntity.getMessage(),
                messageEntity.getSender().getId(),
                messageEntity.getRecipientId(),
                messageEntity.getSendDatetime(),
                messageEntity.wasRead(),
                messageEntity.isDeleted());
    }

    public static MessageEntity mapMessageToSend(final UserEntity user, final String deviceIp, final MessageDto messageDto) {
        return new MessageEntity(
                messageDto.getTopic(),
                messageDto.getMessage(),
                user,
                messageDto.getRecipientId(),
                deviceIp,
                LocalDateTime.now(),
                null,
                MESSAGE_WAS_READ,
                MESSAGE_WAS_DELETED
        );
    }
}
