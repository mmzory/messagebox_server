package pl.manicki.messagebox.data.dto;

import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@NoArgsConstructor
public class MessageDto {
    private long id;

    @NotNull(message = "Any topic has to be provided.")
    @NotEmpty(message = "Any topic has to be provided.")
    @Length(min = 1, max = 80)
    private String topic;

    @NotNull(message = "Any message has to be provided.")
    @NotEmpty(message = "Any message has to be provided.")
    @Length(min = 1)
    private String message;

    private long senderId;

    @NotNull(message = "Recipient Id has to be provided.")
    @Min(value = 1, message = "Recipient ID has to be grater then 0.")
    private long recipientId;

    private LocalDateTime sendDatetime;
    private boolean wasRead;
    private boolean deleted;

    public MessageDto(long id, String topic, long recipientId, long senderId, LocalDateTime sendDatetime, boolean wasRead) {
        this.id = id;
        this.topic = topic;
        this.recipientId = recipientId;
        this.senderId = senderId;
        this.sendDatetime = sendDatetime;
        this.wasRead = wasRead;
    }

    public MessageDto(long id, String topic, String message, long senderId, long recipientId, LocalDateTime sendDatetime, boolean wasRead, boolean deleted) {
        this.id = id;
        this.topic = topic;
        this.message = message;
        this.senderId = senderId;
        this.recipientId = recipientId;
        this.sendDatetime = sendDatetime;
        this.wasRead = wasRead;
        this.deleted = deleted;
    }

    public long getId() {
        return id;
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    public long getSenderId() {
        return senderId;
    }

    public long getRecipientId() {
        return recipientId;
    }

    public LocalDateTime getSendDatetime() {
        return sendDatetime;
    }

    public boolean wasRead() {
        return wasRead;
    }

    public boolean isDeleted() {
        return deleted;
    }
}
