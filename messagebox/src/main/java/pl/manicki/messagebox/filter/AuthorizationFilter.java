package pl.manicki.messagebox.filter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import pl.manicki.commons.data.dto.AuthorizationDetails;
import pl.manicki.commons.data.entity.UserEntity;
import pl.manicki.commons.encoder.UserRolesEncoder;
import pl.manicki.commons.exception.AuthenticationTokenIncorrectException;
import pl.manicki.commons.exception.UserNotFoundException;
import pl.manicki.commons.service.UserTokenService;
import pl.manicki.commons.service.UserTokenServiceImpl;
import pl.manicki.commons.util.JwtHelper;
import pl.manicki.messagebox.exception.AuthorizationException;
import pl.manicki.messagebox.exception.SessionExpiredException;
import pl.manicki.messagebox.repository.UserRepository;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Component
public class AuthorizationFilter extends OncePerRequestFilter {
    private static final Logger logger = LoggerFactory.getLogger(AuthorizationFilter.class);

    private final long USER_ID_HEADER_NOT_PROVIDED = -1L;

    private final JwtHelper jwtHelper;
    private final UserTokenService userTokenService;
    private final UserRepository userRepository;
    private final UserRolesEncoder userRolesEncoder;

    public AuthorizationFilter(UserRepository userRepository) {
        this.jwtHelper = new JwtHelper();
        this.userRolesEncoder = new UserRolesEncoder();
        this.userTokenService = new UserTokenServiceImpl(userRolesEncoder);
        this.userRepository = userRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        final String requestTokenHeader = request.getHeader("Authorization");

        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer")) {
            final String token = requestTokenHeader.substring(7);

            try {
                final long userIdFromHeader = checkUserIdFromHeader(request);
                final Claims claims = jwtHelper.decodeJwtToken(token);
                final long userId = ((Integer) claims.get("id")).longValue();
                final UUID authenticationToken = UUID.fromString((String) claims.get("token"));
                AuthorizationDetails authorizationDetails;

                if (userIdFromHeader != userId) {
                    throw new AuthorizationException(
                            String.format("User tried to authorize with header ID = '%d' and token ID = '%d'.",
                                    userIdFromHeader, userId));
                }

                authorizationDetails = userTokenService.checkUserTokenAndUpdate(userId, authenticationToken);

                if (authorizationDetails == null) {
                    authorizationDetails = checkIfUserIsRegisteredAndHaveValidToken(userId, authenticationToken);
                    userTokenService.saveUserAndToken(userId, authorizationDetails);
                }

                final UsernamePasswordAuthenticationToken userAuthFromToken =
                        new UsernamePasswordAuthenticationToken(claims.get("username"), null, authorizationDetails.getRoles());

                response.addHeader("deviceIp", request.getRemoteAddr());
                response.addHeader("userId", request.getHeader("userId"));

                userAuthFromToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(userAuthFromToken);

            } catch (SignatureException | UserNotFoundException | SessionExpiredException
                    | AuthenticationTokenIncorrectException | ExpiredJwtException | AuthorizationException ex) {
                logger.warn(ex.getMessage());
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
            } catch (MalformedJwtException ex) {
                logger.warn("User provided modified token: {}", token);
            }
        }

        filterChain.doFilter(request, response);
    }

    private long checkUserIdFromHeader(HttpServletRequest request) throws AuthorizationException {
        final String userIdFromHeaderValue = request.getHeader("userId");

        if (userIdFromHeaderValue == null || userIdFromHeaderValue.isEmpty()) {
            return USER_ID_HEADER_NOT_PROVIDED;
        }

        long userIdFromHeader;

        try {
            userIdFromHeader = Long.parseLong(userIdFromHeaderValue);
            if (userIdFromHeader < 1) {
                logger.warn("User tried to authorize with header userId = {}", userIdFromHeader);
                throw new AuthorizationException("Incorrect user.");
            }
        } catch (NumberFormatException ex) {
            logger.warn("User tried to authorize with unsupported header userId = {}", userIdFromHeaderValue);
            throw new AuthorizationException("Incorrect user.");
        }

        return userIdFromHeader;
    }

    private AuthorizationDetails checkIfUserIsRegisteredAndHaveValidToken(final long userId, final UUID token)
            throws SessionExpiredException, AuthenticationTokenIncorrectException {
        final UserEntity userEntity = checkIfUserIsRegistered(userId);
        return checkIfUserHaveValidToken(userEntity, token);
    }

    private UserEntity checkIfUserIsRegistered(final long userId) throws UserNotFoundException {
        final Optional<UserEntity> userEntity = userRepository.findById(userId);

        if (!userEntity.isPresent()) {
            throw new AuthenticationTokenIncorrectException(
                    String.format("User not found in database. User ID = %d", userId));
        }

        return userEntity.get();
    }

    private AuthorizationDetails checkIfUserHaveValidToken(final UserEntity userEntity, final UUID token)
            throws AuthenticationTokenIncorrectException {

        if (!userEntity.getToken().equals(token)) {
            throw new AuthenticationTokenIncorrectException(
                    String.format("User with ID = %d used incorrect token for authorization.", userEntity.getId()));
        }

        if (LocalDateTime.now().isAfter(userEntity.getTokenValidityDateTime())) {
            throw new SessionExpiredException(String.format("Session for user with ID = %d has ended at %s.",
                    userEntity.getId(), userEntity.getTokenValidityDateTime()));
        }

        return new AuthorizationDetails(
                userEntity.getToken(),
                userEntity.getTokenValidityDateTime(),
                userRolesEncoder.decodeRoles(userEntity.getRole()));
    }
}
