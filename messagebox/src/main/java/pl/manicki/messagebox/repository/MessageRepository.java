package pl.manicki.messagebox.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.manicki.commons.data.entity.MessageEntity;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<MessageEntity, Long> {
    @Query("SELECT NEW pl.manicki.commons.data.entity.MessageEntity(m.id, m.topic, m.recipientId, m.sender, m.sendDatetime, m.wasRead, m.deleted) " +
            "FROM MessageEntity m WHERE m.recipientId = :userId AND m.deleted = false")
    List<MessageEntity> getListOfBasicMessagesDetailsFromUser(@Param("userId") long userId);

    MessageEntity findByRecipientIdAndId(long userId, long messageId);
}
