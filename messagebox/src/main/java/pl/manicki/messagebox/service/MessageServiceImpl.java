package pl.manicki.messagebox.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.manicki.commons.data.entity.MessageEntity;
import pl.manicki.commons.data.entity.UserEntity;
import pl.manicki.commons.exception.UserNotFoundException;
import pl.manicki.messagebox.data.dto.MessageDto;
import pl.manicki.messagebox.data.mapper.MessageMapper;
import pl.manicki.messagebox.exception.AuthenticationException;
import pl.manicki.messagebox.exception.MessageNotFoundException;
import pl.manicki.messagebox.repository.MessageRepository;
import pl.manicki.messagebox.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {
    private static final Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);

    private final MessageRepository messageRepository;
    private final UserRepository userRepository;

    public MessageServiceImpl(final MessageRepository messageRepository, UserRepository userRepository) {
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<MessageDto> getListOfBasicMessagesDetailsFromUser(final long userId) {
        final List<MessageDto> messageEntityList =
                MessageMapper.mapBasicListOfMessageEntityToDto(messageRepository.getListOfBasicMessagesDetailsFromUser(userId));

        if (messageEntityList == null) {
            return new ArrayList<>();
        }

        return messageEntityList;
    }

    @Transactional
    @Override
    public MessageDto getMessage(final long userId, final long messageId) {
        final MessageEntity messageEntity = messageRepository.findByRecipientIdAndId(userId, messageId);

        if (messageEntity == null) {
            logger.warn("User with ID = {} tried read a message with ID = {}. Access denied.", userId, messageId);
            throw new AuthenticationException("Access denied!");
        }

        if (!messageEntity.wasRead()) {
            messageEntity.setWasRead(true);
            messageEntity.setReadDatetime(LocalDateTime.now());
            messageRepository.saveAndFlush(messageEntity);
        }

        return MessageMapper.mapMessageEntityToDto(messageEntity);
    }

    @Override
    public void saveMessage(long userId, final String deviceIp, final MessageDto messageDto) {
        final UserEntity userEntity = userRepository.findById(userId).orElseThrow(() -> {
            logger.warn("User with id = {} doesn't exists in database.", userId);
            throw new UserNotFoundException("User not found.");
        });

        final MessageEntity messageEntity = MessageMapper.mapMessageToSend(userEntity, deviceIp, messageDto);

        messageRepository.save(messageEntity);
    }

    @Transactional
    @Override
    public void setMessageAsDeleted(final long userId, final long messageId) {
        final MessageEntity messageEntity = messageRepository.findById(messageId)
                .orElseThrow(() -> new MessageNotFoundException(String.format("Message with ID = %d not found.", messageId)));

        if (messageEntity.getSender().getId() != userId) {
            throw new AuthenticationException(String.format("Message with ID = %d is not assigned to user with ID = %d", messageId, userId));
        }

        messageEntity.setDeleted(true);
        messageEntity.setDeleteDatetime(LocalDateTime.now());

        messageRepository.save(messageEntity);
    }
}
