package pl.manicki.messagebox.service;

import pl.manicki.messagebox.data.dto.MessageDto;

import javax.security.auth.message.AuthException;
import java.util.List;

public interface MessageService {
    List<MessageDto> getListOfBasicMessagesDetailsFromUser(final long userId);
    MessageDto getMessage(final long userId, final long messageId);
    void saveMessage(long recipientId, String deviceIp, MessageDto messageDto);
    void setMessageAsDeleted(final long userId, final long messageId);
}
