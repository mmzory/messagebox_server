package pl.manicki.commons.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.manicki.commons.configuration.TokenConfiguration;
import pl.manicki.commons.data.dto.AuthorizationDetails;
import pl.manicki.commons.data.dto.UserDto;
import pl.manicki.commons.data.entity.UserEntity;
import pl.manicki.commons.encoder.UserRolesEncoder;
import pl.manicki.commons.exception.AuthenticationTokenIncorrectException;
import pl.manicki.commons.repository.UserTokenRepository;
import pl.manicki.commons.util.UserTokenHelper;

import java.time.LocalDateTime;
import java.util.UUID;

public class UserTokenServiceImpl implements UserTokenService {
    private static final Logger logger = LoggerFactory.getLogger(UserTokenServiceImpl.class);

    private final UserTokenRepository userTokenRepository;
    private final UserRolesEncoder userRolesEncoder;

    public UserTokenServiceImpl(UserRolesEncoder userRolesEncoder) {
        this.userRolesEncoder = userRolesEncoder;
        this.userTokenRepository = UserTokenRepository.getInstance();
    }

    @Override
    public AuthorizationDetails generateUserToken(final UserDto userDto) {
        logger.debug("Generating token for user with ID = {}, username = '{}'", userDto.getId(), userDto.getUsername());
        final AuthorizationDetails authorizationDetails = new AuthorizationDetails(
                UserTokenHelper.generateRandomUserToken(),
                LocalDateTime.now().plusHours(TokenConfiguration.TOKEN_EXPIRATION_HOURS),
                userRolesEncoder.decodeRoles(userDto.getRoleCode())
        );

        logger.debug("Generated token = {}\n" +
                "Validity token datetime = {}",
                authorizationDetails.getToken(),
                authorizationDetails.getTokenValidityDateTime());

        if (userTokenRepository.isTokenAlreadyExists(authorizationDetails.getToken())) {
            logger.info("Token {} already exists. Generating new token.", authorizationDetails.getToken());
            generateUserToken(userDto);
        }

        return authorizationDetails;
    }

    @Override
    public void setTokenToUser(final UserEntity userEntity, final AuthorizationDetails authorizationDetails) {
        logger.info("Setting token for user with id = '{}' and username = '{}'.", userEntity.getId(), userEntity.getUsername());
        userEntity.setToken(authorizationDetails.getToken());
        userEntity.setTokenValidityDateTime(authorizationDetails.getTokenValidityDateTime());
    }

    @Override
    public void saveUserAndToken(long userId, final AuthorizationDetails authorizationDetails) {
        userTokenRepository.saveUserAndToken(userId, authorizationDetails);
    }

    @Override
    public AuthorizationDetails checkUserTokenAndUpdate(final long userId, final UUID token) throws AuthenticationTokenIncorrectException {
        if (!userTokenRepository.isUserRegisteredInTokenRepository(userId)) {
            return null;
        }

        final AuthorizationDetails authorizationDetails = userTokenRepository.getUserAuthorizationDetails(userId);
        if (!isUserTokenCorrectAndValid(userId, token, authorizationDetails)) {
            return null;
        }

        saveUserAndToken(userId, new AuthorizationDetails(authorizationDetails.getToken(),
                LocalDateTime.now().plusHours(TokenConfiguration.TOKEN_EXPIRATION_HOURS),
                authorizationDetails.getRoles()));

        return authorizationDetails;
    }

    private boolean isUserTokenCorrectAndValid(long userId, UUID token, AuthorizationDetails authorizationDetails) {
        if (!userTokenRepository.getUserAuthorizationDetails(userId).getToken().equals(token)
                || authorizationDetails.getTokenValidityDateTime().isBefore(LocalDateTime.now())) {
            logger.warn(String.format("User with ID = %d used incorrect token for authorization.", userId));
            logger.info("User data are corrupted. Removing user from local repository.");
            userTokenRepository.removeUserAndToken(userId);
            return false;
        }

        return true;
    }
}
