package pl.manicki.commons.service;

import pl.manicki.commons.data.dto.AuthorizationDetails;
import pl.manicki.commons.data.dto.UserDto;
import pl.manicki.commons.data.entity.UserEntity;
import pl.manicki.commons.exception.AuthenticationTokenIncorrectException;

import java.util.UUID;

public interface UserTokenService {
    AuthorizationDetails generateUserToken(final UserDto userDto);
    void setTokenToUser(final UserEntity userEntity, final AuthorizationDetails authorizationDetails);
    void saveUserAndToken(long userId, AuthorizationDetails authorizationDetails);
    AuthorizationDetails checkUserTokenAndUpdate(final long userId, final UUID token) throws AuthenticationTokenIncorrectException;
}
