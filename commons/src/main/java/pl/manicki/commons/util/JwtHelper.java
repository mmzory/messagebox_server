package pl.manicki.commons.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import pl.manicki.commons.data.dto.AuthorizationDetails;
import pl.manicki.commons.data.dto.UserDto;

import javax.xml.bind.DatatypeConverter;

public class JwtHelper {

    private final String tokenKey = "8ot7L90R#$";

    public String createAuthenticationToken(final UserDto user, final AuthorizationDetails authorizationDetails) {
        return Jwts.builder()
                .claim("id", user.getId())
                .claim("username", user.getUsername())
                .claim("token", authorizationDetails.getToken())
                .signWith(SignatureAlgorithm.HS512, tokenKey)
                .compact();
    }

    public Claims decodeJwtToken(String token) throws RuntimeException {
        return Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(tokenKey))
                .parseClaimsJws(token).getBody();
    }
}
