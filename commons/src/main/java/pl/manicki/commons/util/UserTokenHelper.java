package pl.manicki.commons.util;

import java.util.UUID;

public class UserTokenHelper {

    public static UUID generateRandomUserToken() {
        return UUID.randomUUID();
    }
}
