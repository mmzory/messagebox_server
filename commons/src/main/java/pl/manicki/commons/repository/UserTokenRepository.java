package pl.manicki.commons.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.manicki.commons.data.dto.AuthorizationDetails;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UserTokenRepository {
    private static Logger logger = LoggerFactory.getLogger(UserTokenRepository.class);
    private static UserTokenRepository instance;
    private final Map<Long, AuthorizationDetails> userTokens;

    private UserTokenRepository() {
        this.userTokens = new HashMap<>();
    }

    public static synchronized UserTokenRepository getInstance() {
        return instance == null ? instance = new UserTokenRepository() : instance;
    }

    public void saveUserAndToken(final long userId, final AuthorizationDetails authorizationDetails) {
        userTokens.put(userId, authorizationDetails);
    }

    public AuthorizationDetails getUserAuthorizationDetails(long userId) {
        return userTokens.get(userId);
    }

    public boolean isUserRegisteredInTokenRepository(final long userId) {
        return userTokens.containsKey(userId);
    }

    public boolean isTokenAlreadyExists(final UUID tokenToCheck) {
        return userTokens.values().parallelStream().anyMatch(authenticationDetails -> authenticationDetails.getToken().equals(tokenToCheck));
    }

    public void removeUserAndToken(long userId) {
        userTokens.remove(userId);
    }
}
