package pl.manicki.commons.encoder;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import pl.manicki.commons.enums.UserRolesEnum;
import pl.manicki.commons.exception.IncorrectUserRoleException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
public class UserRolesEncoder {

    public int encodeRoles(String... roles) {
        Arrays.stream(roles).forEach(this::checkIfRoleExists);

        return Arrays.stream(roles)
                .map(UserRolesEnum::valueOf)
                .mapToInt(UserRolesEnum::getRoleCode)
                .sum();
    }

    public Set<SimpleGrantedAuthority> decodeRoles(int roleCodesSum) {
        Set<SimpleGrantedAuthority> decodedRoles = new HashSet<>();

        for (UserRolesEnum currentRole : UserRolesEnum.values()) {
            if (roleCodesSum >= currentRole.getRoleCode()) {
                decodedRoles.add(new SimpleGrantedAuthority(currentRole.getRoleName()));
                roleCodesSum -= currentRole.getRoleCode();
            }
        }

        return decodedRoles;
    }

    private void checkIfRoleExists(String role) throws RuntimeException {
        Arrays.stream(UserRolesEnum.values())
                .filter(userRole -> userRole.name().equals(role))
                .findAny()
                .orElseThrow(() -> new IncorrectUserRoleException("Incorrect role was provided. Set role/roles from available positions list."));
    }
}
