package pl.manicki.commons.data.mapper;

import pl.manicki.commons.data.dto.UserDto;
import pl.manicki.commons.data.entity.UserEntity;

public class UserMapper {

    public static UserDto mapRegisterUserEntityToDto(final UserEntity userEntity) {
        return new UserDto(
                userEntity.getUsername(),
                userEntity.getPassword(),
                userEntity.isActive(),
                userEntity.isBlocked(),
                userEntity.getRole()
        );
    }

    public static UserDto mapEntityToDtoForJwtToken(final UserEntity userEntity) {
        return new UserDto(
                userEntity.getId(),
                userEntity.getUsername(),
                userEntity.isActive(),
                userEntity.isBlocked(),
                userEntity.getRole()
        );
    }
}
