package pl.manicki.commons.data.entity;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor

@Entity
@Table(name = "message")
public class MessageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = 80)
    private String topic;

    @Column(columnDefinition = "TEXT")
    private String message;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sender_id")
    private UserEntity sender;

    @Column(name = "recipient_id", nullable = false)
    private long recipientId;

    @Column(name = "device_ip", nullable = false)
    private String deviceIp;

    @Column(name = "send_datetime", nullable = false)
    private LocalDateTime sendDatetime;

    @Setter
    @Column(name = "was_read")
    @ColumnDefault("0")
    private boolean wasRead = false;

    @Setter
    @Column(name = "read_datetime")
    private LocalDateTime readDatetime;

    @Setter
    @Column(name = "deleted")
    @ColumnDefault("0")
    private boolean deleted = false;

    @Setter
    @Column(name = "delete_datetime")
    private LocalDateTime deleteDatetime;

    public MessageEntity(long id, String topic, long recipientId, UserEntity sender, LocalDateTime sendDatetime, boolean wasRead, boolean deleted) {
        this.id = id;
        this.topic = topic;
        this.recipientId = recipientId;
        this.sender = sender;
        this.sendDatetime = sendDatetime;
        this.wasRead = wasRead;
        this.deleted = deleted;
    }

    public MessageEntity(String topic, String message, UserEntity sender, long recipientId, String deviceIp,
                         LocalDateTime sendDatetime, LocalDateTime deleteDatetime, boolean wasRead, boolean deleted) {
        this.topic = topic;
        this.message = message;
        this.sender = sender;
        this.recipientId = recipientId;
        this.deviceIp = deviceIp;
        this.sendDatetime = sendDatetime;
        this.deleteDatetime = deleteDatetime;
        this.wasRead = wasRead;
        this.deleted = deleted;
    }

    public long getId() {
        return id;
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    public UserEntity getSender() {
        return sender;
    }

    public long getRecipientId() {
        return recipientId;
    }

    public String getDeviceIp() {
        return deviceIp;
    }

    public LocalDateTime getSendDatetime() {
        return sendDatetime;
    }

    public boolean wasRead() {
        return wasRead;
    }

    public LocalDateTime getReadDatetime() {
        return readDatetime;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public LocalDateTime getDeleteDatetime() {
        return deleteDatetime;
    }
}
