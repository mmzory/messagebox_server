package pl.manicki.commons.data.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Getter
@AllArgsConstructor
public class AuthorizationDetails {
    private final UUID token;
    private final LocalDateTime tokenValidityDateTime;
    private final Set<SimpleGrantedAuthority> roles;
}
