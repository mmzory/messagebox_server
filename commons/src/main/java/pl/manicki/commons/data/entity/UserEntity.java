package pl.manicki.commons.data.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@ToString(exclude = "messages")
@EqualsAndHashCode(exclude = "messages")

@Entity
@Table(name = "user")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column
    private UUID token;

    @Column(name = "token_validity_datetime")
    private LocalDateTime tokenValidityDateTime;

    @Column(name = "user_registered_datetime")
    private LocalDateTime userRegisteredDateTime;

    @Column
    private boolean active;

    @Column
    private boolean blocked;

    @Column(name = "role")
    private int role;

    @OneToMany(mappedBy = "sender")
    private List<MessageEntity> messages;

    public UserEntity(String login, String password,
                      UUID token,
                      LocalDateTime userRegisteredDateTime,
                      LocalDateTime tokenValidityDateTime,
                      boolean active, boolean blocked,
                      int role) {
        this.username = login;
        this.password = password;
        this.token = token;
        this.userRegisteredDateTime = userRegisteredDateTime;
        this.tokenValidityDateTime = tokenValidityDateTime;
        this.active = active;
        this.blocked = blocked;
        this.role = role;
    }
}
