package pl.manicki.commons.data.dto;

import lombok.Getter;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@Getter
public class UserDto {
    private long id;
    private final String username;
    private String password;
    private final boolean active;
    private final boolean blocked;
    private final int roleCode;
    private UUID token;
    private LocalDateTime tokenValidityDateTime;

    public UserDto(String username, String password, boolean active, boolean blocked, int roleCode) {
        this.username = username;
        this.password = password;
        this.active = active;
        this.blocked = blocked;
        this.roleCode = roleCode;
    }

    public UserDto(long id, String username, boolean active, boolean blocked, int roleCode) {
        this.id = id;
        this.username = username;
        this.active = active;
        this.blocked = blocked;
        this.roleCode = roleCode;
    }

    public UserDto(long id, String username, boolean active, boolean blocked, int roleCode, UUID token,
                   LocalDateTime tokenValidityDateTime) {
        this.id = id;
        this.username = username;
        this.active = active;
        this.blocked = blocked;
        this.roleCode = roleCode;
        this.token = token;
        this.tokenValidityDateTime = tokenValidityDateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto userDto = (UserDto) o;
        return id == userDto.id && Objects.equals(username, userDto.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username);
    }
}
