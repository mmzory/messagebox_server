package pl.manicki.commons.exception;

public class AuthenticationTokenIncorrectException extends RuntimeException {

    public AuthenticationTokenIncorrectException(String message) {
        super(message);
    }
}
