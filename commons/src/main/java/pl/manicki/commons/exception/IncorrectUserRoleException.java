package pl.manicki.commons.exception;

public class IncorrectUserRoleException extends RuntimeException {

    public IncorrectUserRoleException(String message) {
        super(message);
    }
}
