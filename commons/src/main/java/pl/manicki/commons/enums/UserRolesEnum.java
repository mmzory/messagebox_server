package pl.manicki.commons.enums;

public enum UserRolesEnum {
    ADMIN("ROLE_ADMIN", 32),
    PRESIDENT("ROLE_PRESIDENT", 16),
    MANAGER("ROLE_MANAGER", 8),
    SECRETARY("ROLE_SECRETARY", 4),
    OFFICE("ROLE_OFFICE", 2),
    MB_USER("ROLE_USER", 1)
    ;

    private final String roleName;
    private final int roleCode;

    UserRolesEnum(String roleName, int roleCode) {
        this.roleName = roleName;
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public int getRoleCode() {
        return roleCode;
    }
}
