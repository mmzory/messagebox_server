# MessageBox server
- - -

## General description
<p>It is a project of application for sending messages between users. 
This is a server part of application - backend.</p>

<p>MessageBox basic assumption was to be fully compatible with the NAS servers, so it can be used by home users and small companies.
I'm providing a technical support, however, there is no guarantee that all functionality will work correctly in 100%, 
because of that it is strongly recommended to do a database back-up quite often - especially at the beginning of usage.</p>

<p>This is an open source project, so the code can be modified by third party.
If the code was download not from the original source (the author GitLab repository), I am not able to provide a support.
Any criticism and ideas for improving or expanding the application are welcome.</p>

- - -

## Environment specification
<p>The application has been tested on Windows, linux and NAS server.</p>
<p>In this project is used an external database (installed on NAS server), 
however, a docker image or local DB server can be also used.</p>
<p>How to use docker image to create a MariaDB container is described on the <a href="https://hub.docker.com/_/mariadb/">official DockerHub website</a>.</p>

<p>The description how to install and configure MariaDB local environment can be found on the <a href="https://mariadb.org/">official MariaDB website</a>.</p>

**Remember to adjust the database connection IP address.**

### Windows
<p>
Version: 10 x64 Pro <br>
JDK version: OpenJDK 15.0.1 - code was performed for Java 8 version (for NAS servers usage). <br>
Database: MariaDB 10 on external Synology NAS Server.
</p>

### Linux Ubuntu
<p>
Version: 20.04.2 <br>
JDK version: OpenJDK 14.0.2 - code was performed for Java 8 version (for NAS servers usage). <br>
Database: MariaDB 10 on external Synology NAS Server.
</p>

### NAS server
<p>
NAS server model: Synology DS118.<br>
JDK version: Oracle JDK 8u261 for non-commercial use. <br>
Database: MariaDB 10.
</p>

- - - 

## Building a JAR files
Before the JAR file building remember to adjust the database connection IP address in `application.properties` files. <br>
It is important to build `common` module at first, because there are classes 
that are used in rest of modules. <br>
This is a maven project, so use the following command to build JAR file 
(it is recommended to use the Windows cmd, Linux terminal or GitBash):
- `mvn clean install` - remember to create database and user for the tests 
    (configuration is inside the `application-test.properties` file in `authorization` module)

- - - 

## Database preparation
Create two databases for specified purposes named as presented:
- `message_box` - production
- `message_box_tests` - tests

The queries creating a specified databases are available in [V01_base_configuration.sql](src/main/resources/V01_base_configuration.sql) file.
There should be specified two users with the different permissions:
- Production profile - user should be created with all the grants without `remove` and `drop`.
- Tests profile - user with all grants should be created - to prevent a data conflict situations.

Currently, inside the database connection properties files (production and tests) there are users with the same password.
It is strongly recommended to use two different users with **different passwords**.

Of course the configuration files can be adjusted to your own needs.